//
//  FGSApi.cs
//  FGSApi
//
//  Created by Richard Bate on 24/05/2017.
//
//  Copyright (c) 2017 RB Coding
//
//  This software is provided 'as-is', without any express or implied
//  warranty. In no event will the authors be held liable for any damages
//  arising from the use of this software.
//
//  Permission is granted to anyone to use this software for any purpose,
//  including commercial applications, and to alter it and redistribute it
//  freely, subject to the following restrictions:
//
//  1. The origin of this software must not be misrepresented; you must not
//  claim that you wrote the original software. If you use this software
//  in a product, an acknowledgment in the product documentation would be
//  appreciated but is not required.
//
//  2. Altered source versions must be plainly marked as such, and must not be
//  misrepresented as being the original software.
//
//  3. This notice may not be removed or altered from any source distribution.
//


using System;
using System.Dynamic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Converters;

namespace fgsapiconsole
{
    ///
    /// Singleton class used to access the FGS api.
    ///
    public sealed class FGSApi
    {
        private const string API_URI = "https://api.ffc-environment-agency.fgs.metoffice.gov.uk";

        private static readonly FGSApi instance = new FGSApi();
   
        private FGSApi(){}

        public static FGSApi Instance
        {
            get 
            {
                return instance; 
            }
        }


        public async Task<JObject> GetStatement(int statementId){

            var statement = await GetStatements(id: statementId);

            return statement["statement"].ToObject<JObject>();
        }


        public async Task<JObject> GetLatestStatement(){

            var allStatements = await GetStatements();

            var latestStatement = allStatements["statements"].First();

            return latestStatement.ToObject<JObject>();

        }


        public async Task<JObject> GetStatements(int? id = null){

            using (var client = new HttpClient())
            {
                try
                {
                    string endpoint = !id.HasValue ? "/api/public/statements" : string.Format("/api/public/statements/{0}", id.Value);

                    client.BaseAddress = new Uri(API_URI);
                    var response = await client.GetAsync(endpoint);
                    response.EnsureSuccessStatusCode(); // Throw in not success

                    var stringResponse = await response.Content.ReadAsStringAsync();
        
                    var parsedResponse = JObject.Parse(stringResponse);

                    return parsedResponse;

                }
                catch (HttpRequestException e)
                {
                    Console.WriteLine($"Request exception: {e.Message}");
                }
            }

            return null;    

        }

    }
}