﻿using System;
using System.Reflection;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace fgsapiconsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("FGS Public API Console ({0})", Assembly.GetEntryAssembly().GetName().Version.ToString());
            Console.WriteLine("=================================");
            bool exit = false;


            while (!exit)
            {
                Console.WriteLine("> Select an option: ");
                Console.WriteLine("\t 1/ Display Latest Statement");
                Console.WriteLine("\t 2/ Display statement by id");
                Console.WriteLine("\t 3/ Display ALL statements (most recent 50)");
                Console.WriteLine("\t 0/ Exit");

                string input = Console.ReadLine();

                short option = 0;
                if (short.TryParse(input, out option))
                {
                    switch (option)
                    {
                        case 0:
                            {
                                exit = true;
                                break;
                            }
                        case 1:
                            {

                                var task = FGSApi.Instance.GetLatestStatement();
                                task.Wait();

                                if(task.Result != null){
                                    DisplayStatement(task.Result);
                                }

                                break;
                            }
                            case 2:
                            {
                               

                                int statementId = 0;
                                Match match = null;
                                do
                                {
                                    Console.Write("Enter statement id: ");

                                    string strId = Console.ReadLine();
                                    
                                    match = Regex.Match(strId, @"^\d+$");
                                    
                                    if(match.Success){
                                        statementId = int.Parse(match.Value);
                                        break;
                                    }
                                    
                                    Console.WriteLine("Invalid statement id!");
                                    Console.WriteLine();

                                } while(match == null || !match.Success);

          
                                var task = FGSApi.Instance.GetStatement(statementId);
                                task.Wait();

                                if(task.Result != null){
                                    DisplayStatement(task.Result);
                                }



                                break;
                            }
                            case 3:
                            {

                                var task = FGSApi.Instance.GetStatements();
                                task.Wait();

                                if(task.Result != null){
                                    foreach(var statement in task.Result["statements"]){
                                        Console.WriteLine();

                                        DisplayStatement(statement.ToObject<JObject>());

                                        Console.WriteLine();
                                    }
                                    
                                }

                                break;
                            }
                        default:
                            {
                                Console.WriteLine("Invalid Option!");
                                break;
                            }
                    }
                }
                else
                {
                    Console.WriteLine("Invalid Option!");
                }

                Console.WriteLine();
            }

        }


        
        private static void DisplayStatement(JObject statement)
        {
            Console.WriteLine("*************************");
            Console.WriteLine("FGS Statement ID {0}", statement["id"]);
            Console.WriteLine("*************************");
            Console.WriteLine();
            Console.WriteLine("Details");
            Console.WriteLine("========================");
            Console.WriteLine();
            Console.WriteLine("Issued at: {0}", DateTime.Parse(statement["issued_at"].ToString()).ToString("dd/MM/yyyy HH:MM:ss"));
            Console.WriteLine();
            Console.WriteLine("Trend: Day 1 [{0}], Day 2 [{0}], Day 3 [{0}], Day 4 [{0}], Day 5 [{0}]",
                statement["flood_risk_trend"]["day1"],
                statement["flood_risk_trend"]["day2"],
                statement["flood_risk_trend"]["day3"],
                statement["flood_risk_trend"]["day4"],
                statement["flood_risk_trend"]["day5"]);
            Console.WriteLine();
            Console.WriteLine("Headline: {0}", statement["headline"]);
            Console.WriteLine();
            Console.WriteLine("Public forecast: {0}", statement["public_forecast"]["english_forecast"]);
            Console.WriteLine();
            Console.WriteLine("Assessment of Flood Risk");
            Console.WriteLine("========================");
            Console.WriteLine();

            foreach (var source in statement["sources"].Children<JObject>())
            {
                foreach (var prop in source.Properties())
                {
                    Console.WriteLine("{0}: {1}", prop.Name, prop.Value);
                    Console.WriteLine();
                }
            }
        }
    }

}
