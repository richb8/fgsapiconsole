# FGS Public API #

A simple singleton class written using dotnet core that provides methods to query the FGS public API for flood guidance statements provided by the Flood Forecasting Centre.

Feel free to re-use the FGSApi.cs code within your own applications. Just give RBCoding a mention in your app :-)

The project is a simple dotnet core console application that you can use as an example for integrating the FGSApi.cs file within your own apps.

### What is this repository for? ###

* FGS Public API
* Version 1.0.0.0

### Dependencies ###

* Newtonsoft.Json (10.0.2)

### How do I get set up? ###

To run the console application do the following:

* Download or clone the repo
* Open a command window/shell in the directory containing the source.
* Run the restore command

```
#!dotnet

dotnet restore

```

* Run the app

```
#!dotnet

dotnet run

```

### How do I use the code in my own dotnet core app? ###

Simply take a copy of the FGSApi.cs file.

Please give RBCoding a mention in your app if you do use the code.

### Who do I talk to? ###

* Repo owner or admin [hello@rbcoding.co.uk](hello@rbcoding.co.uk)

### License ###

Copyright (c) 2017 RB Coding

This software is provided 'as-is', without any express or implied
warranty. In no event will the authors be held liable for any damages
arising from the use of this software.

Permission is granted to anyone to use this software for any purpose,
including commercial applications, and to alter it and redistribute it
freely, subject to the following restrictions:

1. The origin of this software must not be misrepresented; you must not
claim that you wrote the original software. If you use this software
in a product, an acknowledgment in the product documentation would be
appreciated but is not required.

2. Altered source versions must be plainly marked as such, and must not be
misrepresented as being the original software.

3. This notice may not be removed or altered from any source distribution.